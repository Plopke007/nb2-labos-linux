# Netwerkbeheer 2 - Linux

Naam cursist: NAAM

## Opgave

De bedoeling van dit labo is een klein netwerk op te zetten met een aantal typische services. Het netwerk heeft IP range 192.168.15.0/24 en de domeinnaam is `linuxlab.lan`.

We streven er naar om heel de opstelling zoveel mogelijk te automatiseren én voldoende generisch te maken, zodat deze configuratie met een minimum aan moeite kan toegepast worden in een andere situatie.

## Overzicht hosts

| Hostnaam | Alias | IP            | Functie                                               |
| :--      | :---  | :---          | :---                                                  |
| srv010   | www   | 192.168.15.10 | Webserver (LAMP-stack met Apache, PHP, Mariadb + CMS) |
| srv011   | file  | 192.168.15.11 | Fileserver (Samba)                                    |
| srv001   | ns1   | 192.168.15.2  | DNS server (BIND)                                     |
| srv002   | ns2   | 192.168.15.3  | Secundaire DNS-server                                 |
| srv003   | dhcp  | 192.168.15.4  | DHCP-server                                           |
| srv012   | mail  | 192.168.15.12 | Mailserver                                           |





