#! /usr/bin/bash
#
# Provisioning script for srv010

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

#
# Variables
#
provisioning_files=/vagrant/provisioning

#
# Common tasks
#
source "${provisioning_files}/common.sh"
